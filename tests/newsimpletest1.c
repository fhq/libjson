/*
 * File:   newsimpletest1.c
 * Author: fcten
 *
 * Created on 2015-11-29, 14:51:26
 */

#include <stdio.h>
#include <stdlib.h>

#include "../libjson.h"

/*
 * Simple C Test Suite
 */

void test1() {
    json_object_t t;
    printf("sizeof json_object_t: %zu\n", sizeof(t));
}

void test2() {
    //printf("newsimpletest1 test 2\n");
    //printf("%%TEST_FAILED%% time=0 testname=test2 (newsimpletest1) message=error message sample\n");

    json_object_t * obj = json_create_object();
    json_object_t * array = json_create_array();
    
    long long l = 123456789;
    int i = 123;
    double d = 0.3;
    float f = 0.5;
    
    json_append(obj, "key1", sizeof("key1")-1, JSON_STRING, "value", sizeof("value")-1);
    json_append(obj, "key2", sizeof("key2")-1, JSON_DOUBLE, &d, 0);
    json_append(obj, "key3", sizeof("key3")-1, JSON_INTEGER, &i, 0);
    json_append(obj, "key4", sizeof("key4")-1, JSON_LONGLONG, &l, 0);
    json_append(obj, "key5", sizeof("key5")-1, JSON_FLOAT, &f, 0);
    json_append(obj, "key6", sizeof("key6")-1, JSON_TRUE, NULL, 0);
    json_append(obj, "key7", sizeof("key7")-1, JSON_FALSE, NULL, 0);
    json_append(obj, "key8", sizeof("key8")-1, JSON_NULL, NULL, 0);
    
    json_append(array, NULL, 0, JSON_STRING, "value", sizeof("value")-1);
    json_append(array, NULL, 0, JSON_DOUBLE, &d, 0);
    json_append(array, NULL, 0, JSON_INTEGER, &i, 0);
    json_append(array, NULL, 0, JSON_TRUE, NULL, 0);
    json_append(array, NULL, 0, JSON_FALSE, NULL, 0);
    json_append(array, NULL, 0, JSON_NULL, NULL, 0);

    json_append(obj, "key9", sizeof("key9")-1, JSON_ARRAY, array, 0);
    
    //for(i=0;i<1000000;i++)
    json_print(obj);
    
    json_delete_object(obj);
    
    printf("\n");
}

int main(int argc, char** argv) {
    printf("%%SUITE_STARTING%% newsimpletest1\n");
    printf("%%SUITE_STARTED%%\n");

    printf("%%TEST_STARTED%% test1 (newsimpletest1)\n");
    test1();
    printf("%%TEST_FINISHED%% time=0 test1 (newsimpletest1) \n");

    printf("%%TEST_STARTED%% test2 (newsimpletest1)\n");
    test2();
    printf("%%TEST_FINISHED%% time=0 test2 (newsimpletest1) \n");

    printf("%%SUITE_FINISHED%% time=0\n");

    return (EXIT_SUCCESS);
}
